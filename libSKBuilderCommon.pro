#-------------------------------------------------
#
# Project created by QtCreator 2013-10-02T22:14:33
#
#-------------------------------------------------

QT       -= gui
TARGET = SKBuilderCommon
CONFIG += c++11
CONFIG += staticlib
TEMPLATE = lib
QMAKE_CXXFLAGS+=-Wunused-but-set-variable


INCLUDEPATH += $$PWD/../external_libs/include/
LIBS += $$PWD/../external_libs/libs/libVariant.a


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a

HEADERS += \
    skbuilder/utils.h \
    skbuilder/input.h \
    skbuilder/input_variant.h \
    skbuilder/factory.h \
    skbuilder/input_nctu.h \
    skbuilder/input_bccms.h

SOURCES += \
    src/utils.cpp \
    src/input.cpp \
    src/factory.cpp \
    src/input_nctu.cpp \
    src/input_bccms.cpp
