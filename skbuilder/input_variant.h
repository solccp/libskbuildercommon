#ifndef TWNINPUT_JSON_H
#define TWNINPUT_JSON_H

#include <Variant/Variant.h>
#include <variantqt.h>
#include "input.h"

#include "input_nctu.h"
#include "factory.h"

namespace libvariant
{
    template<>
    struct VariantConvertor<SKBuilder::Input::AtomicInfo>
    {
        static libvariant::Variant toVariant(const SKBuilder::Input::AtomicInfo &rhs)
        {
            libvariant::Variant var;
            var["hubbard"] = VariantConvertor<QMap<QString, double> >::toVariant(rhs.hubbard);
            var["occupation"] = VariantConvertor<QMap<QString, double> >::toVariant(rhs.occupation);
            if (!rhs.final_occupation.isEmpty())
                var["final_occupation"] = VariantConvertor<QMap<QString, double> >::toVariant(rhs.final_occupation);
            var["onecenter_template_input"] = VariantConvertor<QString>::toVariant(rhs.onecenter_input);
            var["orbital_energy"] = VariantConvertor<QMap<QString, double> >::toVariant(rhs.orbitalEnergy);
            var["orbitals"] = VariantConvertor<QList<QStringList> >::toVariant(rhs.orbitals);
            return var;
        }
        static void fromVariant(const libvariant::Variant &var, SKBuilder::Input::AtomicInfo &rhs)
        {
                using map_str_double = QMap<QString, double>;
                GET_VARIANT(rhs.hubbard,       var, "hubbard",        map_str_double);
                GET_VARIANT(rhs.occupation,    var, "occupation",     map_str_double);

                GET_VARIANT(rhs.orbitalEnergy, var, "orbital_energy", map_str_double);

                bool hasValue;
                GET_VARIANT_OPT(rhs.onecenter_input, var, "onecenter_template_input", QString, hasValue);
                GET_VARIANT_OPT(rhs.final_occupation,    var, "final_occupation",     map_str_double, hasValue);
                Q_UNUSED(hasValue)
                using orbital_lists = QList<QStringList>;
                GET_VARIANT(rhs.orbitals, var, "orbitals", orbital_lists);

                using map_str_str = QMap<QString, QString>;
                GET_VARIANT_OPT(rhs.orbital_files, var, "orbital_files", map_str_str, hasValue);
                GET_VARIANT_OPT(rhs.potential_file, var, "potential_file", QString, hasValue);
                GET_VARIANT_OPT(rhs.density_file, var, "density_file", QString, hasValue);


        }
    };

    template<>
    struct VariantConvertor<SKBuilder::Input::Confining>
    {
        static libvariant::Variant toVariant(const SKBuilder::Input::Confining &rhs)
        {
            libvariant::Variant var;
            var["orbital_types"] = VariantConvertor<QStringList>::toVariant(rhs.orbital_types);
            var["parameters"] = rhs.parameters->toVariant();
            return var;
        }
        static void fromVariant_1(const libvariant::Variant &var, SKBuilder::Input::Confining &rhs,
                                  const QString& toolchain_name)
        {
            GET_VARIANT(rhs.orbital_types, var, "orbital_types", QStringList);
            try
            {               
                if (toolchain_name == "mixed")
                {
                    rhs.parameters = SKBuilder::Input::ConfiningParameterFactory::make("nctu");
                }
                else
                {
                    rhs.parameters = SKBuilder::Input::ConfiningParameterFactory::make(toolchain_name);
                }

                rhs.parameters->fromVariant(var["parameters"]);
            }
            catch (KeywordNotFound &e)
            {
                KeywordNotFound error(std::string("parameters/") + e.what());
                throw error;
            }
            catch (KeywordParseError &e)
            {
                KeywordParseError error(std::string("parameters/") + e.what());
                throw error;
            }
        }
    };




    template<>
    struct VariantConvertor<SKBuilder::Input::ConfiningAction>
    {
        static libvariant::Variant toVariant(const SKBuilder::Input::ConfiningAction &rhs)
        {
            libvariant::Variant var;
            var["take"] = VariantConvertor<QStringList>::toVariant(rhs.take);
            var["confining"] = VariantConvertor<QList<SKBuilder::Input::Confining> >::toVariant(rhs.confinings);
            return var;
        }
        static void fromVariant_1(const libvariant::Variant &var, SKBuilder::Input::ConfiningAction &rhs, const QString& toolchain_name)
        {
            GET_VARIANT(rhs.take, var, "take", QStringList);
            using list = QList<SKBuilder::Input::Confining>;
            GET_VARIANT_1(rhs.confinings, var, "confining", list, toolchain_name);
        }
    };




    template<>
    struct VariantConvertor<SKBuilder::Input::ConfiningInfo>
    {
        static libvariant::Variant toVariant(const SKBuilder::Input::ConfiningInfo &rhs)
        {
            libvariant::Variant var;
            var["confining_actions"] = VariantConvertor<QList<SKBuilder::Input::ConfiningAction> >::
                    toVariant(rhs.confining_actions);
            return var;
        }
        static void fromVariant_1(const libvariant::Variant &var, SKBuilder::Input::ConfiningInfo &rhs,
                                  const QString& toolchain_name)
        {
            GET_VARIANT_1(rhs.confining_actions, var, "confining_actions", QList<SKBuilder::Input::ConfiningAction>, toolchain_name);
        }
    };

    template<>
    struct VariantConvertor<SKBuilder::Input::Option>
    {
        static libvariant::Variant toVariant(const SKBuilder::Input::Option &rhs)
        {
            libvariant::Variant var;
            var["save_intermediate"] = rhs.saveIntermediate;            
            var["output_prefix"] = rhs.outputPrefix.toStdString();
            return var;
        }
        static void fromVariant(const libvariant::Variant &var, SKBuilder::Input::Option &rhs)
        {
            bool hasValue;
            GET_VARIANT_OPT(rhs.saveIntermediate, var, "save_intermediate", bool, hasValue);
            GET_VARIANT_OPT(rhs.outputPrefix, var, "output_prefix", QString, hasValue);
            Q_UNUSED(hasValue);
        }
    };




    template<>
    struct VariantConvertor<SKBuilder::Input::OneCenterInfo>
    {
        static libvariant::Variant toVariant(const SKBuilder::Input::OneCenterInfo &rhs)
        {
            libvariant::Variant var;
            var["path"] = rhs.path.toStdString();
            libvariant::Variant var_options;

            if (rhs.default_parameter)
            {
                var_options["default"] = rhs.default_parameter->toVariant();
            }

            libvariant::Variant specific_node;

            for(auto kv = rhs.specific_parameters.begin(); kv != rhs.specific_parameters.end() ; ++kv)
            {
                specific_node[kv->first.toStdString()] = kv->second->toVariant();
            }
            var_options["specific"] = specific_node;

            var["options"] = var_options;
            return var;
        }


        static void fromVariant_1(const libvariant::Variant &var, SKBuilder::Input::OneCenterInfo &rhs, const QString& toolchain_name)
        {
            GET_VARIANT(rhs.path, var, "path", QString);
            if (var.Contains("options"))
            {
                auto var_options = var["options"];
                try
                {
                    if (var_options.Contains("default"))
                    {
                        rhs.default_parameter = SKBuilder::Input::OneCenterParameterFactory::make(toolchain_name);
                        rhs.default_parameter->fromVariant(var_options["default"]);
                    }
                }
                catch (KeywordNotFound &e)
                {
                    KeywordNotFound error(std::string("default/") + e.what());
                    throw error;
                }
                catch (KeywordParseError &e)
                {
                    KeywordParseError error(std::string("default/") + e.what());
                    throw error;
                }


                if (var_options.Contains("specific"))
                {
                    for(auto const & kv : var_options["specific"].AsMap())
                    {
                        try
                        {
                            auto item = SKBuilder::Input::OneCenterParameterFactory::make(toolchain_name);
                            item->fromVariant(kv.second);
                            rhs.specific_parameters.insert(std::make_pair(QString::fromStdString(kv.first), item));
                        }
                        catch (KeywordNotFound &e)
                        {
                            KeywordNotFound error(std::string("specific/") +kv.first +std::string("/")+ e.what());
                            throw error;
                        }
                        catch (KeywordParseError &e)
                        {
                            KeywordParseError error(std::string("specific/") +kv.first +std::string("/")+ e.what());
                            throw error;
                        }
                    }
                }
            }  
        }
    };


    template<>
    struct VariantConvertor<SKBuilder::Input::TwoCenterInfo>
    {
        static libvariant::Variant toVariant(const SKBuilder::Input::TwoCenterInfo &rhs)
        {
            libvariant::Variant var;
            var["path"] = rhs.path.toStdString();
            if (rhs.superposition == SKBuilder::Input::TwoCenterInfo::SuperPositionType::PotentialSuperposition)
            {
                var["superposition"] = "potential";
            }
            else
            {
                var["superposition"] = "density";
            }


            libvariant::Variant var_options;

            if (rhs.default_parameter)
            {
                var_options["default"] = rhs.default_parameter->toVariant();
            }

            libvariant::Variant specific_node;

            for(auto kv = rhs.specific_parameters.begin(); kv != rhs.specific_parameters.end() ; ++kv)
            {
                specific_node[kv->first.toStdString()] = kv->second->toVariant();
            }
            var_options["specific"] = specific_node;

            var["options"] = var_options;
            return var;
        }


        static void fromVariant_1(const libvariant::Variant &var, SKBuilder::Input::TwoCenterInfo &rhs, const QString& toolchain_name)
        {
            GET_VARIANT(rhs.path, var, "path", QString);
            bool hasValue;
            QString super;
            GET_VARIANT_OPT(super, var, "superposition", QString, hasValue);
            if (hasValue)
            {
                if (super.toLower() == "potential")
                {
                    rhs.superposition = SKBuilder::Input::TwoCenterInfo::SuperPositionType::PotentialSuperposition;
                }
                else
                {
                    rhs.superposition = SKBuilder::Input::TwoCenterInfo::SuperPositionType::DensitySuperposition;
                }
            }

            if (var.Contains("options"))
            {
                auto var_options = var["options"];
                if (var_options.Contains("default"))
                {
                    try
                    {
                        rhs.default_parameter = SKBuilder::Input::TwoCenterParameterFactory::make(toolchain_name);
                        rhs.default_parameter->fromVariant(var_options["default"]);
                    }
                    catch (KeywordNotFound &e)
                    {
                        KeywordNotFound error(std::string("default/") + e.what());
                        throw error;
                    }
                    catch (KeywordParseError &e)
                    {
                        KeywordParseError error(std::string("default/") + e.what());
                        throw error;
                    }
                }

                if (var_options.Contains("specific"))
                {
                    for(auto const & kv : var_options["specific"].AsMap())
                    {
                        try
                        {
                            auto item = SKBuilder::Input::TwoCenterParameterFactory::make(toolchain_name);
                            item->fromVariant(kv.second);
                            rhs.specific_parameters.insert(std::make_pair(QString::fromStdString(kv.first), item));
                        }
                        catch (KeywordNotFound &e)
                        {
                            KeywordNotFound error(std::string("specific/") +kv.first +std::string("/")+ e.what());
                            throw error;
                        }
                        catch (KeywordParseError &e)
                        {
                            KeywordParseError error(std::string("specific/") +kv.first +std::string("/")+ e.what());
                            throw error;
                        }
                    }
                }

            }
        }
    };


    template<>
    struct VariantConvertor<SKBuilder::Input::ToolchainInfo>
    {
        static libvariant::Variant toVariant(const SKBuilder::Input::ToolchainInfo &rhs)
        {
            libvariant::Variant var;
            var["name"] = rhs.name.toStdString();
            var["onecenter"] = VariantConvertor<SKBuilder::Input::OneCenterInfo>::toVariant(rhs.onecent_info);
            var["twocenter"] = VariantConvertor<SKBuilder::Input::TwoCenterInfo>::toVariant(rhs.twocent_info);

            return var;
        }
        static void fromVariant(const libvariant::Variant &var, SKBuilder::Input::ToolchainInfo &rhs)
        {
            GET_VARIANT(rhs.name, var, "name", QString);

            if (rhs.name == "mixed")
            {
                GET_VARIANT_1(rhs.onecent_info, var, "onecenter", SKBuilder::Input::OneCenterInfo, "nctu");
                GET_VARIANT_1(rhs.twocent_info, var, "twocenter", SKBuilder::Input::TwoCenterInfo, "bccms");
            }
            else
            {
                GET_VARIANT_1(rhs.onecent_info, var, "onecenter", SKBuilder::Input::OneCenterInfo, rhs.name);
                GET_VARIANT_1(rhs.twocent_info, var, "twocenter", SKBuilder::Input::TwoCenterInfo, rhs.name);
            }

        }
    };

    template<>
    struct VariantConvertor<SKBuilder::Input::SKBuilderInput>
    {
        static libvariant::Variant toVariant(const SKBuilder::Input::SKBuilderInput &rhs)
        {
            libvariant::Variant var;
            var["atomic_info"] = VariantConvertor<QMap<QString, SKBuilder::Input::AtomicInfo> >::toVariant(rhs.atomicinfo);
            var["confining_info"] = VariantConvertor<QMap<QString, SKBuilder::Input::ConfiningInfo> >::toVariant(rhs.confininginfo);
            var["options"] = VariantConvertor<SKBuilder::Input::Option>::toVariant(rhs.options);

            var["toolchain_info"] = VariantConvertor<SKBuilder::Input::ToolchainInfo>::toVariant(rhs.toolchain_info);

            if (rhs.all_pairs)
            {
                var["desired_pairs"] = "all";
            }
            else
            {
                var["desired_pairs"] = VariantConvertor<QList<QString> >::toVariant(rhs.desired_pairs);
            }

            var["external_repulsive_potentials"] = VariantConvertor<QMap<QString, QString> >::toVariant(rhs.external_repulsive_potentials);

            return var;
        }
        static void fromVariant(const libvariant::Variant &var, SKBuilder::Input::SKBuilderInput &rhs)
        {
            try
            {
                using atomic_info_map = QMap<QString, SKBuilder::Input::AtomicInfo>;
                GET_VARIANT(rhs.atomicinfo, var, "atomic_info", atomic_info_map);

                GET_VARIANT(rhs.toolchain_info, var, "toolchain_info", SKBuilder::Input::ToolchainInfo);

                using confining_info_map = QMap<QString, SKBuilder::Input::ConfiningInfo>;

                bool hasValue;
                GET_VARIANT_1_OPT(rhs.confininginfo, var, "confining_info", confining_info_map, hasValue, rhs.toolchain_info.name);

                if (var["desired_pairs"].IsString() && var["desired_pairs"].AsString() == "all")
                {
                    rhs.all_pairs = true;
                }
                else
                {
                    using pair_list = QList<QString>;
                    GET_VARIANT(rhs.desired_pairs, var, "desired_pairs", pair_list);
                }



                typedef QMap<QString, QString> mss;
                GET_VARIANT_OPT(rhs.external_repulsive_potentials, var, "external_repulsive_potentials", mss, hasValue);
                GET_VARIANT_OPT(rhs.options, var, "options", SKBuilder::Input::Option, hasValue);
                Q_UNUSED(hasValue);
            }
            catch (KeywordNotFound &e)
            {
                throw KeywordNotFound(std::string("Keyword not found: ") + e.what());
            }
            catch (KeywordParseError &e)
            {
                throw KeywordParseError(std::string("Parse error: ") + e.what());
            }
        }

    };
}


#endif // TWNINPUT_JSON_H

