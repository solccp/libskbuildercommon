#ifndef INPUT_BCCMS_H
#define INPUT_BCCMS_H

#include "input.h"


namespace SKBuilder
{
    namespace Input
    {
        class OneCenterParameter_BCCMS : public OneCenterParameter
        {
        public:
            int functional = 3; // PBE
            int max_scf = 1000;
            bool zora = false;

            bool automatic_exponents = true;
            double minimal_component = 0.1;
            int number_exponents = 4;

            int power = 2;
            std::vector<double> exponents;

            bool broyden = true;
            double broyden_factor = 0.1;
        public:
            virtual QString name() const;
            virtual libvariant::Variant toVariant() const;
            virtual void fromVariant(const libvariant::Variant &var);
            virtual std::shared_ptr<OneCenterParameter> clone() const;

            // OneCenterParameter interface
        public:
            QString getFunctionalName() const override;
        };

        class TwoCenterParameter_BCCMS : public TwoCenterParameter
        {
        public:
            double end_condition = 5.0e-5;
            double interval = 0.1;
            double end_distance = 12.0;
            int ngrid1 = 300;
            int ngrid2 = 100;
            QString scheme = "density_pbe";

        public:
            virtual QString name() const;
            virtual libvariant::Variant toVariant() const;
            virtual void fromVariant(const libvariant::Variant &var);
            virtual std::shared_ptr<TwoCenterParameter> clone() const;
        };


        class ConfiningParameter_BCCMS : public ConfiningParameter
        {
        public:
            double r = 2.7;
            double n = 2.0;
            ConfiningParameter_BCCMS() = default;
            ConfiningParameter_BCCMS(const ConfiningParameter_BCCMS& rhs) = delete;

        public:
            virtual QString name() const;
            virtual libvariant::Variant toVariant() const;
            virtual void fromVariant(const libvariant::Variant &var);
            virtual std::shared_ptr<ConfiningParameter> clone() const override;
            virtual bool operator ==(const ConfiningParameter &other) const;
        };
    }
}


#endif // INPUT_NCTU_H

