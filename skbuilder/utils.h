#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QPair>


QPair<int, int> getOrbitalQuantumNumbers(const QString& orbital);
int getOrbitalLQuantumNumber(const QString& orbital);
QString getOrbitalNameFromQuantumNumber(int n, int l);

QString which(const QString& exec);

bool writeFile(const QString& filename, const QString& content);

QStringList readFileIntoStringList(const QString& filename);
QString readFileIntoString(const QString& filename);

#endif // UTILS_H



