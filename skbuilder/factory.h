#ifndef FACTORY_H_c3de5740_7544_45f1_997d_00185a32de8f
#define FACTORY_H_c3de5740_7544_45f1_997d_00185a32de8f

#include <memory>
#include "skbuilder/input.h"

namespace SKBuilder
{
    namespace Input
    {
        struct ConfiningParameterFactory
        {
            static std::shared_ptr<ConfiningParameter> make(const QString& keyword);
        };

        struct OneCenterParameterFactory
        {
            static std::shared_ptr<OneCenterParameter> make(const QString& keyword);
        };

        struct TwoCenterParameterFactory
        {
            static std::shared_ptr<TwoCenterParameter> make(const QString& keyword);
        };

    }
}


#endif // FACTORY_H

