#ifndef INPUT_H_42fcf40b_3509_40d9_8b2f_509795a0a703
#define INPUT_H_42fcf40b_3509_40d9_8b2f_509795a0a703

#include <QString>
#include <QVariant>
#include <QMap>
#include <QDebug>
#include <QDir>

#include <memory>
#include <map>

#include <Variant/Variant.h>

namespace SKBuilder
{
    namespace Input
    {

        class Option
        {
        public:
            QString outputPrefix = "";
            bool saveIntermediate = false;
            bool debug = false;
        };


        class BaseParameter
        {
        public:
            virtual ~BaseParameter(){}
            virtual QString name() const = 0;
            virtual libvariant::Variant toVariant() const = 0 ;
            virtual void fromVariant(const libvariant::Variant& var) = 0;
        };


        class TwoCenterParameter : public BaseParameter
        {
        public:
            virtual ~TwoCenterParameter(){}
            virtual std::shared_ptr<TwoCenterParameter> clone() const = 0;
        };

        class OneCenterParameter : public BaseParameter
        {
        public:
            virtual ~OneCenterParameter(){}
            virtual std::shared_ptr<OneCenterParameter> clone() const = 0;
            virtual QString getFunctionalName() const = 0;
        };

        class TwoCenterInfo
        {
        public:
            QString path;
            enum class SuperPositionType : int
            {
                PotentialSuperposition = 0x1,
                DensitySuperposition = 0x2
            };
            SuperPositionType superposition = SuperPositionType::DensitySuperposition;
            std::map<QString, std::shared_ptr<TwoCenterParameter>> specific_parameters;
            std::shared_ptr<TwoCenterParameter> default_parameter;
        };

        class OneCenterInfo
        {
        public:
            QString path = "onecent";
            std::map<QString, std::shared_ptr<OneCenterParameter>> specific_parameters;
            std::shared_ptr<OneCenterParameter> default_parameter;
        };

        class ToolchainInfo
        {
        public:
            QString name;
            OneCenterInfo onecent_info;
            TwoCenterInfo twocent_info;
        };


        class ConfiningParameter : public BaseParameter
        {
        public:
            ConfiningParameter();
            ConfiningParameter(const ConfiningParameter& rhs) = delete;
            virtual ~ConfiningParameter();
            virtual std::shared_ptr<ConfiningParameter> clone() const = 0;
            virtual bool operator ==(const ConfiningParameter& other) const = 0;
        };

        class Confining
        {
        public:
            QStringList orbital_types;
            std::shared_ptr<ConfiningParameter> parameters;
            Confining();
            Confining(const Confining& rhs);
            virtual ~Confining();
            bool operator ==(const Confining& other) const;
        };

        struct ConfiningAction
        {
            QStringList take;
            QList<Confining> confinings;
            bool operator ==(const ConfiningAction& other) const;
        };

        struct ConfiningInfo
        {
            QList<ConfiningAction> confining_actions;
        };

        class AtomicInfo
        {
        public:
            QString onecenter_input = "";
            QList<QStringList> orbitals;
            QMap<QString, double> hubbard;
            QMap<QString, double> orbitalEnergy;
            QMap<QString, double> occupation;
            QMap<QString, double> final_occupation;

            QMap<QString, QString> orbital_files;
            QString density_file;
            QString potential_file;
            void makeAbsoluatePath(const QDir& rootDir);
        };

        class SKBuilderInput
        {
        public:
            SKBuilderInput() = default;
            SKBuilderInput(const SKBuilderInput& rhs) = default;
//            SKBuilderInput(const SKBuilderInput& rhs) = delete;
        public:
            Option options;
            ToolchainInfo toolchain_info;
            QMap<QString, AtomicInfo> atomicinfo;
            QMap<QString, ConfiningInfo> confininginfo;
            QMap<QString, QString> external_repulsive_potentials;
        public:
            QList<QString> desired_pairs;
            bool all_pairs = false;
            void makeAbsoluatePath(const QDir& rootDir);
        };
    }
}
#endif // INPUT_H_42fcf40b_3509_40d9_8b2f_509795a0a703

