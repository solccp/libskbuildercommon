#ifndef INPUT_NCTU_H
#define INPUT_NCTU_H

#include "input.h"


namespace SKBuilder
{
    namespace Input
    {
        class OneCenterParameter_NCTU : public OneCenterParameter
        {
        public:
            QString functional = "pbe";
            int ngrid = 3000;

            // BaseParameter interface
        public:
            virtual QString name() const;
            virtual libvariant::Variant toVariant() const;
            virtual void fromVariant(const libvariant::Variant &var);

            // OneCenterParameter interface
        public:
            virtual std::shared_ptr<OneCenterParameter> clone() const;

            // OneCenterParameter interface
        public:
            QString getFunctionalName() const override;
        };

        class TwoCenterParameter_NCTU : public TwoCenterParameter
        {
        public:
            int ngrid1 = 200;
            int ngrid2 = 200;
            double rm = 0.4;
            double c2 = -0.25;
            double end_distance = 15.0;
//            double start_distance = 0.02;
            double interval = 0.02;

            QString functional_type = "gga";
            QString functional_name = "pbe";

            // BaseParameter interface
        public:
            virtual QString name() const;
            virtual libvariant::Variant toVariant() const;
            virtual void fromVariant(const libvariant::Variant &var);

            // TwoCenterParameter interface
        public:
            virtual std::shared_ptr<TwoCenterParameter> clone() const;
        };


        class ConfiningParameter_NCTU : public ConfiningParameter
        {
        public:
            double w = 0.0;
            double a = 3.0;
            double r = 2.7;
            ConfiningParameter_NCTU() = default;
            ConfiningParameter_NCTU(const ConfiningParameter_NCTU& rhs) = delete;

            // BaseParameter interface
        public:
            virtual QString name() const;
            virtual libvariant::Variant toVariant() const;
            virtual void fromVariant(const libvariant::Variant &var);

            // ConfiningParameter interface
        public:
            virtual std::shared_ptr<ConfiningParameter> clone() const override;

            // ConfiningParameter interface
        public:
            virtual bool operator ==(const ConfiningParameter &other) const;
        };
    }
}


#endif // INPUT_NCTU_H

