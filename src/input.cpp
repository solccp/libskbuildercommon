
#include <QDir>
#include "utils.h"
#include "skbuilder/input.h"

namespace SKBuilder
{

namespace Input
{

Confining::Confining()
{
    qDebug() << "Confining ctor" << this;
}

Confining::Confining(const Input::Confining &rhs)
{
    qDebug() << "Confining copy_ctor" << this;
    orbital_types = rhs.orbital_types;
    parameters = rhs.parameters->clone();
}

Confining::~Confining()
{
    qDebug() << "Confining::dtor" << this;
}

bool Confining::operator ==(const Input::Confining &other) const
{
    if ( orbital_types != other.orbital_types )
    {
        return false;
    }

    if (parameters == other.parameters)
    {
        return true;
    }
    else
    {
        return (*parameters == *other.parameters);
    }
}

bool ConfiningAction::operator ==(const Input::ConfiningAction &other) const
{
    return (take == other.take && confinings == other.confinings);
}

ConfiningParameter::ConfiningParameter()
{
    qDebug() << "ConfiningParameter::ctor" << this;
}

ConfiningParameter::~ConfiningParameter()
{
    qDebug() << "ConfiningParameter::dtor" << this;
}

void SKBuilderInput::makeAbsoluatePath(const QDir &rootDir)
{
    QMutableMapIterator<QString, AtomicInfo> it(this->atomicinfo);
    while(it.hasNext())
    {
        it.next();
        AtomicInfo &atomic_info = it.value();
        atomic_info.makeAbsoluatePath(rootDir);
    }
}

void AtomicInfo::makeAbsoluatePath(const QDir &rootDir)
{
    if (!this->density_file.isEmpty())
    {
        ADPT::makeAbsolutePath_local(rootDir, density_file);
    }
    if (!potential_file.isEmpty())
    {
        ADPT::makeAbsolutePath_local(rootDir, potential_file);
    }
    QMutableMapIterator<QString, QString> it(orbital_files);
    while(it.hasNext())
    {
        it.next();
        ADPT::makeAbsolutePath_local(rootDir, it.value());
    }
}


}
}

