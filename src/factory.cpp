#include "skbuilder/factory.h"

#include "skbuilder/input_nctu.h"
#include "skbuilder/input_bccms.h"

namespace SKBuilder
{

    namespace Input
    {

    std::shared_ptr<ConfiningParameter> ConfiningParameterFactory::make(const QString &keyword)
    {
        if (keyword.toLower() == "nctu")
        {
            auto p = std::make_shared<ConfiningParameter_NCTU>();
            return std::static_pointer_cast<ConfiningParameter>(p);
        }
        else if (keyword.toLower() == "bccms")
        {
            auto p = std::make_shared<ConfiningParameter_BCCMS>();
            return std::static_pointer_cast<ConfiningParameter>(p);
        }

        throw std::runtime_error(keyword.toStdString() + " type of OneCenterParameter is not implemented.");
    }

    std::shared_ptr<OneCenterParameter> OneCenterParameterFactory::make(const QString &keyword)
    {
        if (keyword.toLower() == "nctu")
        {
            auto p = std::make_shared<OneCenterParameter_NCTU>();
            return std::static_pointer_cast<OneCenterParameter>(p);
        }
        else if (keyword.toLower() == "bccms")
        {
            auto p = std::make_shared<OneCenterParameter_BCCMS>();
            return std::static_pointer_cast<OneCenterParameter>(p);
        }
        throw std::runtime_error(keyword.toStdString() + " type of OneCenterParameter is not implemented.");
    }
    std::shared_ptr<TwoCenterParameter> TwoCenterParameterFactory::make(const QString &keyword)
    {
        if (keyword.toLower() == "nctu")
        {
            auto p = std::make_shared<TwoCenterParameter_NCTU>();
            return std::static_pointer_cast<TwoCenterParameter>(p);
        }
        else if (keyword.toLower() == "bccms")
        {
            auto p = std::make_shared<TwoCenterParameter_BCCMS>();
            return std::static_pointer_cast<TwoCenterParameter>(p);
        }
        throw std::runtime_error(keyword.toStdString() + " type of TwoCenterParameter is not implemented.");
    }


    }
}
