#include "skbuilder/input_nctu.h"

#include <Variant/Variant.h>
#include <variantqt.h>

namespace SKBuilder
{
    namespace Input
    {
        QString OneCenterParameter_NCTU::name() const
        {
            return "nctu";
        }

        libvariant::Variant OneCenterParameter_NCTU::toVariant() const
        {
            libvariant::Variant var;
            var["ngrid"] = ngrid;
            var["functional"] = functional.toLower().toStdString();
            return var;
        }

        void OneCenterParameter_NCTU::fromVariant(const libvariant::Variant &var)
        {
            bool hasValue;
            GET_VARIANT_OPT(this->ngrid, var, "ngrid", int, hasValue);
            GET_VARIANT_OPT(this->functional, var, "functional", QString, hasValue);
            Q_UNUSED(hasValue);
        }

        std::shared_ptr<OneCenterParameter> OneCenterParameter_NCTU::clone() const
        {
            auto new_ptr = std::make_shared<OneCenterParameter_NCTU>(*this);
            return std::static_pointer_cast<OneCenterParameter>(new_ptr);
        }







        QString TwoCenterParameter_NCTU::name() const
        {
            return "nctu";
        }

        libvariant::Variant TwoCenterParameter_NCTU::toVariant() const
        {
            libvariant::Variant var;
            var["ngrid1"] = this->ngrid1;
            var["ngrid2"] = this->ngrid2;
            var["rm"] = this->rm;
            var["c2"] = this->c2;
//            var["start_distance"] = this->start_distance;
            var["end_distance"] = this->end_distance;
            var["interval"] = this->interval;

            var["functional_type"] = this->functional_type.toLower().toStdString();
            var["functional_name"] = this->functional_name.toLower().toStdString();
            return var;
        }

        void TwoCenterParameter_NCTU::fromVariant(const libvariant::Variant &var)
        {
            bool hasValue;
            GET_VARIANT_OPT(this->ngrid1, var, "ngrid1", int, hasValue);
            GET_VARIANT_OPT(this->ngrid2, var, "ngrid2", int, hasValue);

            GET_VARIANT_OPT(this->rm, var, "rm", double, hasValue);
            GET_VARIANT_OPT(this->c2, var, "c2", double, hasValue);
//            GET_VARIANT_OPT(this->start_distance, var, "start_distance", double, hasValue);
            GET_VARIANT_OPT(this->end_distance, var, "end_distance", double, hasValue);
            GET_VARIANT_OPT(this->interval, var, "interval", double, hasValue);


            GET_VARIANT_OPT(this->functional_type, var, "functional_type", QString, hasValue);
            GET_VARIANT_OPT(this->functional_name, var, "functional_name", QString, hasValue);
            Q_UNUSED(hasValue);
        }

        std::shared_ptr<TwoCenterParameter> TwoCenterParameter_NCTU::clone() const
        {
            auto new_ptr = std::make_shared<TwoCenterParameter_NCTU>(*this);
            return std::static_pointer_cast<TwoCenterParameter>(new_ptr);
        }


//        ConfiningParameter_NCTU::ConfiningParameter_NCTU(const ConfiningParameter_NCTU &rhs)
//        {
//            r = rhs.r;
//            w = rhs.w;
//            a = rhs.a;
//        }

        QString ConfiningParameter_NCTU::name() const
        {
            return "nctu";
        }

        libvariant::Variant ConfiningParameter_NCTU::toVariant() const
        {
            libvariant::Variant var;
            var["w"] = this->w;
            var["a"] = this->a;
            var["r"] = this->r;
            return var;
        }

        void ConfiningParameter_NCTU::fromVariant(const libvariant::Variant &var)
        {
            GET_VARIANT(this->w, var, "w", double);
            GET_VARIANT(this->a, var, "a", double);
            GET_VARIANT(this->r, var, "r", double);
        }


        std::shared_ptr<ConfiningParameter> ConfiningParameter_NCTU::clone() const
        {
            auto new_ptr = std::make_shared<ConfiningParameter_NCTU>();
            new_ptr->w = this->w;
            new_ptr->a = this->a;
            new_ptr->r = this->r;

            return std::static_pointer_cast<ConfiningParameter>(new_ptr);
        }

        bool ConfiningParameter_NCTU::operator ==(const ConfiningParameter &other) const
        {
            auto myptr = dynamic_cast<const ConfiningParameter_NCTU*>(&other);
            if (myptr == nullptr)
            {
                return false;
            }
            return ( (w == myptr->w) && (r == myptr->r) && (a == myptr->a));
        }

    }
}

QString SKBuilder::Input::OneCenterParameter_NCTU::getFunctionalName() const
{
    return this->functional.toUpper();
}
