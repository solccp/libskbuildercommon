#include "skbuilder/input_bccms.h"

#include <Variant/Variant.h>
#include <variantqt.h>

namespace SKBuilder
{
    namespace Input
    {
        QString OneCenterParameter_BCCMS::name() const
        {
            return "bccms";
        }

        libvariant::Variant OneCenterParameter_BCCMS::toVariant() const
        {
            libvariant::Variant var;
            var["functional"] = this->functional;
            var["zora"] = this->zora;
            var["broyden"] = this->broyden;
            var["broyden_factor"] = this->broyden_factor;
            var["max_scf"] = this->max_scf;

            libvariant::Variant subvar;
            if (this->automatic_exponents)
            {
                subvar["auto"] = true;
                subvar["minimal_exponents"] = this->minimal_component;
                subvar["number_exponents"] = this->number_exponents;
            }
            else
            {
                subvar["auto"] = false;
                subvar["exponents"] = libvariant::VariantConvertor<std::vector<double> >::toVariant(this->exponents);
            }
            subvar["power"] = this->power;
            var["exponent_definition"] = subvar;
            return var;
        }

        void OneCenterParameter_BCCMS::fromVariant(const libvariant::Variant &var)
        {
            bool hasValue;
            GET_VARIANT_OPT(this->functional, var, "functional", int, hasValue);

            GET_VARIANT_OPT(this->zora, var, "zora", bool, hasValue);
            GET_VARIANT_OPT(this->broyden, var, "broyden", bool, hasValue);
            GET_VARIANT_OPT(this->broyden_factor, var, "broyden_factor", double, hasValue);
            GET_VARIANT_OPT(this->max_scf, var, "max_scf", int, hasValue);

            if (var.Contains("exponent_definition"))
            {
                libvariant::Variant subvar = var["exponent_definition"];
                GET_VARIANT(automatic_exponents, subvar, "auto", bool);
                if (automatic_exponents)
                {
                    GET_VARIANT_OPT(minimal_component, subvar, "minimal_exponents", double, hasValue);
                    GET_VARIANT_OPT(number_exponents, subvar, "number_exponents", int, hasValue);
                    GET_VARIANT_OPT(power, subvar, "power", int, hasValue);
                }
                else
                {
                    GET_VARIANT(exponents, subvar, "exponents", std::vector<double>);
                    GET_VARIANT_OPT(power, subvar, "power", int, hasValue);
                }
            }

            Q_UNUSED(hasValue);
        }

        std::shared_ptr<OneCenterParameter> OneCenterParameter_BCCMS::clone() const
        {
            auto new_ptr = std::make_shared<OneCenterParameter_BCCMS>(*this);
            return std::static_pointer_cast<OneCenterParameter>(new_ptr);
        }







        QString TwoCenterParameter_BCCMS::name() const
        {
            return "bccms";
        }

        libvariant::Variant TwoCenterParameter_BCCMS::toVariant() const
        {
            libvariant::Variant var;
            var["ngrid1"] = this->ngrid1;
            var["ngrid2"] = this->ngrid2;

            var["end_distance"] = this->end_distance;
            var["interval"] = this->interval;
            var["end_condition"] = this->end_condition;
            var["scheme"] = this->scheme.toStdString();
            return var;
        }

        void TwoCenterParameter_BCCMS::fromVariant(const libvariant::Variant &var)
        {
            bool hasValue;
            GET_VARIANT_OPT(this->ngrid1, var, "ngrid1", int, hasValue);
            GET_VARIANT_OPT(this->ngrid2, var, "ngrid2", int, hasValue);

            GET_VARIANT_OPT(this->end_distance, var, "end_distance", double, hasValue);
            GET_VARIANT_OPT(this->end_condition, var, "end_condition", double, hasValue);
            GET_VARIANT_OPT(this->interval, var, "interval", double, hasValue);
            GET_VARIANT_OPT(this->scheme, var, "scheme", QString, hasValue);

            Q_UNUSED(hasValue);
        }

        std::shared_ptr<TwoCenterParameter> TwoCenterParameter_BCCMS::clone() const
        {
            auto new_ptr = std::make_shared<TwoCenterParameter_BCCMS>(*this);
            return std::static_pointer_cast<TwoCenterParameter>(new_ptr);
        }


        QString ConfiningParameter_BCCMS::name() const
        {
            return "bccms";
        }

        libvariant::Variant ConfiningParameter_BCCMS::toVariant() const
        {
            libvariant::Variant var;
            var["n"] = this->n;
            var["r"] = this->r;
            return var;
        }

        void ConfiningParameter_BCCMS::fromVariant(const libvariant::Variant &var)
        {
            GET_VARIANT(this->n, var, "n", double);
            GET_VARIANT(this->r, var, "r", double);
        }


        std::shared_ptr<ConfiningParameter> ConfiningParameter_BCCMS::clone() const
        {
            auto new_ptr = std::make_shared<ConfiningParameter_BCCMS>();
            new_ptr->n = this->n;
            new_ptr->r = this->r;

            return std::static_pointer_cast<ConfiningParameter>(new_ptr);
        }

        bool ConfiningParameter_BCCMS::operator ==(const ConfiningParameter &other) const
        {
            auto myptr = dynamic_cast<const ConfiningParameter_BCCMS*>(&other);
            if (myptr == nullptr)
            {
                return false;
            }
            return ( (n == myptr->n) && (r == myptr->r));
        }

    }
}


QString SKBuilder::Input::OneCenterParameter_BCCMS::getFunctionalName() const
{
    switch (this->functional)
    {
        case 0:
            return "HF";
        case 1:
            return "X-Alpha";
        case 2:
            return "LDA";
        case 3:
            return "PBE";

    }
}
