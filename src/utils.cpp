#include "skbuilder/utils.h"
#include <QRegExp>
#include <QDebug>
#include <QStringList>
#include <QString>
#include <QFile>
#include <QProcessEnvironment>
#include <QFileInfo>
#include <QDir>

const static QString l_qns = "spdfghi";

QPair<int, int> getOrbitalQuantumNumbers(const QString &orbital)
{

    QPair<int, int> res(-1,-1);
    QRegExp rx("^(\\d+)(\\w)$");
    if (rx.exactMatch(orbital))
    {
        int n = rx.capturedTexts().at(1).toInt();
        int l = l_qns.indexOf(rx.capturedTexts().at(2).at(0));
        res.first = n;
        res.second = l;
        if (l == -1 || l >= n)
        {
            res.first = -1;
            res.second = -1;
        }
    }
    return res;
}

int getOrbitalLQuantumNumber(const QString& orbital)
{
    QPair<int, int> tmp = getOrbitalQuantumNumbers(orbital);
    if (tmp.first != -1)
    {
        return tmp.second;
    }
    return l_qns.indexOf(orbital.at(0));
}
QString which(const QString& exec)
{
    if (QFileInfo(exec).isAbsolute())
    {
        return exec;
    }
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    QStringList pathes = env.value("PATH").split(":") ;
    for(int i=0; i<pathes.size();++i)
    {
        QDir path = QDir(pathes[i]);
        QFileInfo info = QFileInfo(path.absoluteFilePath(exec));
        if (info.exists() && info.isExecutable())
        {
            return info.absoluteFilePath();
        }
    }

    QFileInfo info = QFileInfo(exec);
    return info.absoluteFilePath();
}


bool writeFile(const QString &filename, const QString &content)
{
    QFile file(filename);
    if (file.open(QFile::WriteOnly))
    {
        QTextStream stream(&file);
        stream << content << endl;
        return true;
    }
    return false;
}


QStringList readFileIntoStringList(const QString& filename)
{
    QStringList list;
    if (QFileInfo(filename).exists())
    {
        QFile file(filename);
        if (!file.open(QIODevice::ReadOnly))
        {
            return list;
        }
        QTextStream stream(&file);
        while(!stream.atEnd())
        {
            list.append(stream.readLine());
        }
    }
    return list;
}
QString readFileIntoString(const QString& filename)
{
    QStringList list = readFileIntoStringList(filename);
    //list.append("\n");
    return list.join("\n");
}


QString getOrbitalNameFromQuantumNumber(int n, int l)
{
    return QString("%1%2").arg(n).arg(l_qns[l]);
}
